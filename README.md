# GitLab CI Community Day

## Resources

- [Event](https://www.meetup.com/gitlab-virtual-meetups/)
- [Slides](https://docs.google.com/presentation/d/1oJLEL2OVm0gz8qvIlqpoidGdmsGt1exT2pwbZjaVTaE/edit?usp=sharing)

The slides provide the step-by-step instructions as exercises for this repository.

- CI/CD Getting Started
- Security scanning
